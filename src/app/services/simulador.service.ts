import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Options } from '../models/options';
import { Global } from '../global';

@Injectable()
export class SimuladorService {

  constructor(private http: HttpClient) { }
  private url = Global.url + 'api/simulador';

  simular(options: Options){
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Headers': 'origin, x-requested-with, content-type, authorization',
        'Access-Control-Allow-Methods': 'PUT, GET, POST, DELETE, OPTIONS',
        'Access-Control-Allow-Credentials': 'true'
      })
    };
    return this.http.post(this.url, options, {responseType: 'text'});
  }

}
